package cat.itb.diceroller;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    private ImageView textGambling;
    private Button buttonDice;
    private Button buttonRestart;
    private ImageView textNumberDice;
    private ImageView textNumberDicetwo;
    public int[] numberDiceCase;
    public int randomNumber;
    public int randomNumbertwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textGambling = findViewById(R.id.textGambling);
        buttonDice = findViewById(R.id.buttonDice);
        buttonRestart = findViewById(R.id.buttonRestartDice);
        textNumberDice = findViewById(R.id.numberDice);
        textNumberDicetwo = findViewById(R.id.numberDicetwo);
        numberDiceCase = new int[]{R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};

        buttonDice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomNumber = new Random().nextInt((6 - 1) + 1);
                randomNumbertwo = new Random().nextInt((6 - 1) + 1);
                textNumberDice.setImageResource(numberDiceCase[randomNumber]);
                textNumberDicetwo.setImageResource(numberDiceCase[randomNumbertwo]);

                jackpot();
            }

        });


        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textNumberDicetwo.setImageResource(R.drawable.empty_dice);
                textNumberDice.setImageResource(R.drawable.empty_dice);
            }
        });

        textNumberDice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomNumber = new Random().nextInt((6 - 1) + 1);
                textNumberDice.setImageResource(numberDiceCase[randomNumber]);
                jackpot();
            }
        });
        textNumberDicetwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomNumbertwo = new Random().nextInt((6 - 1) + 1);
                textNumberDicetwo.setImageResource(numberDiceCase[randomNumbertwo]);
                jackpot();
            }
        });


    }

    public void jackpot() {
        if (randomNumber == 5 && randomNumbertwo == 5) {
            Toast toast = Toast.makeText(MainActivity.this, "JACKPOT!", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.TOP, 0, 0);
            toast.show();
        }
    }
}